#include <gazebo/gazebo.hh>

namespace gazebo
{
  class WorldPluginTutorial : public WorldPlugin
  {
    public: WorldPluginTutorial() : WorldPlugin()
            {
              printf("Hello World!\n");
            }

    public: void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
            {
              printf("Hello World2222!\n");
            }
  };
  GZ_REGISTER_WORLD_PLUGIN(WorldPluginTutorial)
}
